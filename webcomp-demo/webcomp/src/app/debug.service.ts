import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DebugService {
  debugModeActive = false;
  tabInfo: string[] = [];

  get debugMode(): boolean {
    return this.debugModeActive;
  }

  set debugMode(value: boolean) {
    this.debugModeActive = value;
  }
}
