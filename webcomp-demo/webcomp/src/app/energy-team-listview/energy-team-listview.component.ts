import {Component, OnInit, ViewChild} from '@angular/core';
import {Team, TeamService} from './team.service';
import {EnergyListheaderComponent} from '../base/energy-listheader/energy-listheader.component';
import {MessageService} from '../message.service';

@Component({
  selector: 'app-energy-team-listview',
  templateUrl: './energy-team-listview.component.html',
  styleUrls: ['./energy-team-listview.component.scss']
})

export class EnergyTeamListviewComponent implements OnInit {

  @ViewChild(EnergyListheaderComponent) listHeader: any;

  tableData!: Team[];
  selectedRowId?: number;
  displayedColumns = ['name', 'description', 'modified', 'created'];

  constructor(private teamService: TeamService, private messageService: MessageService) { }

  formatDate(utc: number): string {
    if (utc === null) {
      return '';
    }
    return new Date(utc).toLocaleString('de');
  }


  ngOnInit(): void {
    this.teamService.getList().subscribe(teams => {
      this.tableData = teams;
      this.listHeader.setCount(teams.length);
    });
  }

  selectRow(row: Team): void {
    this.selectedRowId = row.id;
  }

  updateParamValues(values: Map<string, string>): void {
    console.log('updateParamValues called', values);
  }

  showDetails(row: Team): void {
    const vid = 'teamDetailView';
    const param = {name: 'teamId', value: row.id};
    this.messageService.sendEvent('ujcommand', JSON.stringify({vid, param}));
  }

}

