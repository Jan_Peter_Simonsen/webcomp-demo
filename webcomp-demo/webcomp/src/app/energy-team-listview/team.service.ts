import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Constants} from '../constants';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  listUrl = Constants.SERVER + 'team/list';

  constructor(private http: HttpClient) { }

  getList(): Observable<Team[]> {
    return this.http.get<Team[]>(this.listUrl);
  }

}

export interface Team {
  id: number;
  name: string;
  description?: string;
  creation: number;
  modified?: number;
}
