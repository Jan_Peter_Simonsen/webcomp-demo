import { Component, OnInit } from '@angular/core';
import {MessageService} from '../message.service';
import {DebugService} from '../debug.service';

@Component({
  selector: 'app-energy-techdemo-view',
  templateUrl: './energy-techdemo-view.component.html',
  styleUrls: ['./energy-techdemo-view.component.css']
})
export class EnergyTechdemoViewComponent implements OnInit {

  contentTabs = ['First', 'Second', 'Third'];
  activeContentTab = this.contentTabs[0];

  modelId = 'no model id';

  constructor(private messageService: MessageService, public debugService: DebugService) { }

  ngOnInit(): void {
  }

  userActivateContentTab(contentTab: string): void {
    const targetContentIndex = this.contentTabs.indexOf(contentTab);
    const param = 'activeContentIndex';
    const paramValue = String(targetContentIndex);
    this.messageService.sendEvent('setParam', JSON.stringify({param, paramValue}));
  }

  ujAction(action: string, actionParam: string): void {
    this.messageService.sendEvent('ujAction', JSON.stringify({action, actionParam}));
  }

  updateParamValues(values: Map<string, string>): void {
    console.log('updateParamValues called', values);
    if (values.get('activeContentIndex')) {
      this.activeContentTab = this.contentTabs[Number(values.get('activeContentIndex'))];
    } else {
      this.activeContentTab = this.contentTabs[0];
    }
    this.modelId = values.get('modelId') || 'no model id';
  }


}
