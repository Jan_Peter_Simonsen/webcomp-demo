import {Injector, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { EnergyWebcomponentComponent } from './energy-webcomponent/energy-webcomponent.component';
import {FormsModule} from '@angular/forms';
import { createCustomElement } from '@angular/elements';
import { EnergyTaskListviewComponent } from './energy-task-listview/energy-task-listview.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material/table';
import {MatButtonModule} from '@angular/material/button';
import {EnergyDirective} from './energy-webcomponent/energy.directive';
import { EnergyListheaderComponent } from './base/energy-listheader/energy-listheader.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {HttpClientModule} from '@angular/common/http';
import { EnergyTeamListviewComponent } from './energy-team-listview/energy-team-listview.component';
import { EnergyTechdemoViewComponent } from './energy-techdemo-view/energy-techdemo-view.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatMenuModule} from '@angular/material/menu';
import {MatTabsModule} from '@angular/material/tabs';
import { EnergyTeamDetailviewComponent } from './energy-team-detailview/energy-team-detailview.component';

@NgModule({
  declarations: [
    EnergyWebcomponentComponent,
    EnergyTaskListviewComponent,
    EnergyDirective,
    EnergyListheaderComponent,
    EnergyTeamListviewComponent,
    EnergyTechdemoViewComponent,
    EnergyTeamDetailviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    FontAwesomeModule,
    HttpClientModule,
    MatMenuModule
  ],
  providers: [],
  entryComponents: [EnergyWebcomponentComponent]
})
export class AppModule {
  constructor(injector: Injector) {
    const el = createCustomElement(EnergyWebcomponentComponent, { injector });
    customElements.define('app-energy-webcomponent', el);
  }

  ngDoBootstrap(): void {}
}
