import {Component, OnInit, ViewChild} from '@angular/core';
import {faFilter, faEllipsisH, faSearch} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-energy-listheader',
  templateUrl: './energy-listheader.component.html',
  styleUrls: ['./energy-listheader.component.scss']
})
export class EnergyListheaderComponent implements OnInit {

  constructor() {
  }

  filters = [
    {value: 'all', viewValue: 'ALL'}
  ];

  selectedFilter = 'all';

  counter = 0;

  icons = {
    faFilter,
    faEllipsisH,
    faSearch
  };

  classNames = {
    listheader: 'listheader',
    filterContainer: 'filterContainer',
    filterIconContainer: 'filterIconContainer',
    counter: 'counter',
    searchIconContainer: 'searchIconContainer',
    menuButton: 'menuButton',
    dotMenu: 'dotMenu'
  };

  setCount(count: number): void {
    this.counter = count;
  }

  ngOnInit(): void {
  }

}
