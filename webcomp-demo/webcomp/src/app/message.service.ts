import {Injectable} from '@angular/core';

export class CustomEventDetail {
  public ownerId = '';
  public componentId = '';
  public command = '';
  public content = '';
}

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private componentId?: string;
  private ownerId?: string;

  static formatCustomEventDetail(detail: CustomEventDetail): string {
    return `Command: "${detail.command}", Content: "${detail.content}"`
      + ` (OwnerId: "${detail.ownerId}", ComponentId: "${detail.componentId}")`;
  }

  sendEvent(command: string, content: string): void {
    if (this.ownerId == null || this.componentId == null) {
      throw new Error('MessageService is not initialized');
    }
    const detail = new CustomEventDetail();
    detail.command = command;
    detail.content = content;
    detail.ownerId = this.ownerId as string;
    detail.componentId = this.componentId as string;
    console.log('Energy-Webcomponent: sending Event: ' + MessageService.formatCustomEventDetail(detail));
    window.dispatchEvent(new CustomEvent('fromWebcomp', {detail}));
  }

  init(componentId: string, ownerId: string): void {
    if (this.ownerId != null || this.componentId != null) {
      throw new Error('MessageService already initialized');
    }
    this.componentId = componentId;
    this.ownerId = ownerId;
  }
}
