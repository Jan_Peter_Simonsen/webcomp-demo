import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appEnergyHost]',
})
export class EnergyDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
