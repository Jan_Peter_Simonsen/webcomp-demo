import {
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation,
  ViewRef
} from '@angular/core';
import {EnergyTaskListviewComponent} from '../energy-task-listview/energy-task-listview.component';
import {EnergyDirective} from './energy.directive';
import {EnergyTeamListviewComponent} from '../energy-team-listview/energy-team-listview.component';
import {ComponentType} from '@angular/cdk/overlay';
import {EnergyTechdemoViewComponent} from '../energy-techdemo-view/energy-techdemo-view.component';
import {CustomEventDetail, MessageService} from '../message.service';
import {DebugService} from '../debug.service';
import {EnergyTeamDetailviewComponent} from '../energy-team-detailview/energy-team-detailview.component';
import {Constants} from '../constants';

@Component({
  selector: 'app-energy-webcomponent',
  templateUrl: './energy-webcomponent.component.html',
  styleUrls: ['./energy-webcomponent.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})

export class EnergyWebcomponentComponent implements OnInit {

  constructor(private componentFactoryResolver: ComponentFactoryResolver,
              private messageService: MessageService,
              public debugService: DebugService) {}

  @Input() componentId = '';
  @Input() ownerId = '';
  @Input() debugmode = '';
  @Input() host = '';

  @ViewChild(EnergyDirective, {static: true}) appEnergyHost!: EnergyDirective;

  message = 'Initial Empty Webcomponent State';
  currentTabId = '';

  // view per tabId is needed to be reinserted / detached at the central viewRef "appEnergyHost"
  viewRefs: Map<string, Map<string, ViewRef>> = new Map();
  // component per tabId is needed to be able to call methods when params change
  componentRefs: Map<string, Map<string, ComponentRef<any>>> = new Map();

  ngOnInit(): void {
    // listen to events
    window.addEventListener('toWebcomp', this.messageFromOutside.bind(this));

    this.messageService.init(this.componentId, this.ownerId);

    // send initial event to inform host app that component is ready
    this.messageService.sendEvent('hello', '');

    this.debugService.debugModeActive = this.debugmode === '1';

    if (this.host !== '') {
      Constants.SERVER = this.host;
    }
  }

  messageFromOutside(params: any): void {
    const detail = params.detail as CustomEventDetail;
    const forMe = detail.componentId === this.componentId;
    const info = forMe ? 'For me!' : 'Not for me';
    this.message = 'Got a message from outside (' + info + '): ' + MessageService.formatCustomEventDetail(detail);
    if (forMe) {
      this.consumeMessage(detail);
    }
  }

  private getComponentTypeByVid(vid: string): ComponentType<any> {
    if (vid === 'taskView') {
      return EnergyTaskListviewComponent;
    }
    if (vid === 'teamView') {
      return EnergyTeamListviewComponent;
    }
    if (vid === 'techView') {
      return EnergyTechdemoViewComponent;
    }
    if (vid === 'teamDetailView') {
      return EnergyTeamDetailviewComponent;
    }
    throw new Error('Unknown vid');
  }

  private consumeMessage(msg: CustomEventDetail): void {
    if (msg.command === 'params') {
      const params = this.parseParams(msg.content);
      const vid = params.values.get('vid') || '';
      this.message = 'show tabId ' + params.tabId + ' & vidId = ' + vid;
      console.log(params.values);

      if (this.currentTabId !== params.tabId) {
        this.switchTab(params.tabId, params.values.get('vid') as string);
      }
      const map = this.componentRefs.get(params.tabId);
      if (map) {
        (map.get(vid) as ComponentRef<any>).instance.updateParamValues(params.values);
      } else {
        throw new Error('no map');
      }
    }
    if (msg.command === 'closeTab') {
      const params = this.parseParams(msg.content);
      this.closeTab(params.tabId);
    }
    this.updateDebugTabInfo();
  }

  private closeTab(tabId: string): void {
    console.log('Close Tab: ' + tabId);
    const map = this.viewRefs.get(tabId);
    if (map) {
      for (const [key, value] of map) {
        value.destroy();
      }
      this.viewRefs.delete(tabId);
    }

    const compMap = this.componentRefs.get(tabId);
    if (compMap) {
      for (const [key, value] of compMap) {
        value.destroy();
      }
      this.componentRefs.delete(tabId);
    }
  }

  private switchTab(tabId: string, vid: string): void {
    if (this.viewRefs.get(tabId)?.has(vid)) {
      // show existing tab
      const viewContainerRef = this.appEnergyHost.viewContainerRef;
      if (viewContainerRef.length > 0) {
        viewContainerRef.detach(0);
      }
      const map = this.viewRefs.get(tabId);
      if (map) {
        const viewRef = map.get(vid);
        viewContainerRef.insert(viewRef as ViewRef);
      } else {
        throw new Error('no map');
      }
    } else {
      // create new tab matching
      const component = this.getComponentTypeByVid(vid);

      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(component);
      const viewContainerRef = this.appEnergyHost.viewContainerRef;
      if (viewContainerRef.length > 0) {
        viewContainerRef.detach(0);
      }
      const componentRef = viewContainerRef.createComponent(componentFactory);
      let viewMap = this.viewRefs.get(tabId);
      if (!viewMap) {
        viewMap = new Map<string, ViewRef>();
        this.viewRefs.set(tabId, viewMap);
      }
      viewMap.set(vid, viewContainerRef.get(0) as ViewRef);

      let compMap = this.componentRefs.get(tabId);
      if (!compMap) {
        compMap = new Map<string, ComponentRef<any>>();
        this.componentRefs.set(tabId, compMap);
      }
      compMap.set(vid, componentRef);
    }
  }

  private updateDebugTabInfo(): void {
    this.debugService.tabInfo = [];
    for (const [key, value] of this.componentRefs) {
      let vids = '';
      for (const [ckey, cvalue] of value) {
        vids += ckey + ', ';
      }
      this.debugService.tabInfo.push(key + ': ' + vids);
    }
  }

  private parseParams(paramStr: string): Params {
    if (!paramStr || paramStr.length < 4) {
      throw new Error(`invalid paramStr: " + $(paramStr)`);
    }
    const params = new Params();
    params.tabId = paramStr.substr(0, 4);
    const questionMarkIndex = paramStr.indexOf('?');
    if (questionMarkIndex !== -1) {
      const paramPart = paramStr.substr(questionMarkIndex + 1);
      const parts = paramPart.split('&');
      parts.forEach(s => {
        const part = s.split('=');
        params.values.set(part[0], part[1]);
      });
    }
    return params;
  }

}

class Params {
  public tabId = '';
  public values: Map<string, string> = new Map();
}
