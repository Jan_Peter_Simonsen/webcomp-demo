import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-energy-team-detailview',
  templateUrl: './energy-team-detailview.component.html',
  styleUrls: ['./energy-team-detailview.component.css']
})
export class EnergyTeamDetailviewComponent implements OnInit {

  teamId = '';

  constructor() { }

  ngOnInit(): void {
  }

  updateParamValues(values: Map<string, string>): void {
    this.teamId = values.get('teamId') || '';
  }

}
