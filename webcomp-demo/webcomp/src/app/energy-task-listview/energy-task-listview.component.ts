import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Task, TaskService} from './task.service';
import {EnergyListheaderComponent} from '../base/energy-listheader/energy-listheader.component';

@Component({
  selector: 'app-energy-task-listview',
  templateUrl: './energy-task-listview.component.html',
  styleUrls: ['./energy-task-listview.component.scss']
})

export class EnergyTaskListviewComponent implements OnInit, OnDestroy {

  @ViewChild(EnergyListheaderComponent) listHeader: any;

  tableData!: Task[];
  selectedRowId?: number;
  displayedColumns = ['name', 'description', 'modified', 'created'];

  constructor(private taskService: TaskService) { }

  formatDate(utc: number): string {
    if (utc === null) {
      return '';
    }
    return new Date(utc).toLocaleString('de');
  }

  ngOnInit(): void {
    this.taskService.getList().subscribe(tasks => {
      this.tableData = tasks;
      this.listHeader.setCount(tasks.length);
    });
  }

  selectRow(row: Task): void {
    this.selectedRowId = row.id;
  }

  updateParamValues(values: Map<string, string>): void {
    console.log('updateParamValues called', values);
  }

  ngOnDestroy(): void {
    // just to make sure, that closing tabs actually destroys components
    console.log('Task-Listview is destroyed');
  }

}

