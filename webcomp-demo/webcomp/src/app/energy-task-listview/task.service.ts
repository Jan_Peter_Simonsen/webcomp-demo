import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Constants} from '../constants';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  listUrl = Constants.SERVER + 'task/list';

  constructor(private http: HttpClient) { }

  getList(): Observable<Task[]> {
    return this.http.get<Task[]>(this.listUrl);
  }

}

export interface Task {
  id: number;
  name: string;
  description?: string;
  creation: number;
  modified?: number;
}
