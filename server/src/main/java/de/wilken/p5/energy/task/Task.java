package de.wilken.p5.energy.task;

import de.wilken.p5.energy.team.Team;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Task extends PanacheEntity {
  public String name;
  public String description;
  public TaskPriority priority;
  public Date creation;
  public Date modified;
  @ManyToOne
  public Team team;
}
