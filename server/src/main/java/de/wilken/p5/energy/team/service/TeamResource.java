package de.wilken.p5.energy.team.service;

import de.wilken.p5.energy.team.Team;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import java.util.Date;
import java.util.List;

@Path("/team")
@Tag(name="Teams")
public class TeamResource {

  @GET
  @Transactional
  @Path("list")
  public List<Team> list() {
    return Team.listAll();
  }

  @GET
  @Transactional
  public Team load(@QueryParam("id") Long id) {
    return Team.findById(id);
  }

  @POST
  @Transactional
  public void add(Team team) {
    team.id = null;
    Team.persist(team);
  }

  @POST
  @Transactional
  @Path("update")
  public void update(Team team) {
    Team existing = Team.findById(team.id);
    existing.name = team.name;
    existing.description = team.description;
    existing.modified = new Date();
    Team.persist(existing);
  }

  @DELETE
  @Transactional
  public void delete(Long teamId) {
    Team.deleteById(teamId);
  }

}
