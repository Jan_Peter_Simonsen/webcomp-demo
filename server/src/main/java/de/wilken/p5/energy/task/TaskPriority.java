package de.wilken.p5.energy.task;

public enum TaskPriority {
  NORMAL, HIGH, LOW
}
