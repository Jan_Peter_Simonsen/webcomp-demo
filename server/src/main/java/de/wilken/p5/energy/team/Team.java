package de.wilken.p5.energy.team;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import java.util.Date;

@Entity
public class Team extends PanacheEntity {
  public String name;
  public String description;
  public Date creation;
  public Date modified;
}
