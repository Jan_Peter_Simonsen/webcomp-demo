package de.wilken.p5.energy.task.service;

import de.wilken.p5.energy.task.Task;
import de.wilken.p5.energy.task.TaskPriority;

import java.util.Date;

public class TaskDto {
  public Long id;
  public Long teamId;
  public String name;
  public String description;
  public TaskPriority priority;
  public Date creation;
  public Date modified;
  public String teamName;

  public TaskDto(Task task) {
    this.id = task.id;
    this.name = task.name;
    this.description = task.description;
    this.priority = task.priority;
    this.creation = task.creation;
    this.modified = task.modified;
    this.teamName = task.team.name;
    this.teamId = task.team.id;
  }
}
