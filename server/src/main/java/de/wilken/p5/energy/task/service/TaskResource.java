package de.wilken.p5.energy.task.service;

import de.wilken.p5.energy.task.Task;
import de.wilken.p5.energy.team.Team;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import java.util.List;
import java.util.stream.Collectors;

@Path("/task")
@Tag(name="Tasks")
public class TaskResource {

  @GET
  @Transactional
  @Path("list")
  public List<TaskDto> list() {
    return Task.listAll().stream().map(
        entity -> new TaskDto((Task) entity)
    ).collect(Collectors.toList());
  }

  @GET
  @Transactional
  public TaskDto load(@QueryParam("id") Long id) {
    Task task = Task.findById(id);
    TaskDto dto = new TaskDto(task);
    return dto;
  }

  @POST
  @Transactional
  public void add(Task task) {
    task.id = null;
    task.team = Team.findById(task.team.id);
    Task.persist(task);
  }

  @DELETE
  @Transactional
  public void delete(Task task) {
    Task.deleteById(task.id);
  }

}
